<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Person Page</title>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9
        }
    </style>


    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <script>
        $(document).ready(function () {
            $("#person").submit(function (event) {


                const firstName = $("#firstName").val();

                const lastName = $("#lastName").val();

                const age = $("#age").val();

                const email = $("#email").val();

                const country = $("#country").val();


                validateNameField(firstName, event);
                validateNameField(lastName, event);
                validateEmailField(email, event);

                function validateNameField(name, event) {
                    highlight($(this), isValidName(name));
                    if (!isValidName(name)) {
                        event.preventDefault();
                        $("#firstName-feedback").text("Please enter at least two characters");
                    }
                }
                function validateEmailField(email, event) {
                    highlight($(this), isValidName(email));
                    if (!validateEmail(email)) {
                        event.preventDefault();
                        $("#email-feedback").text("Please enter a valid email");
                    }
                }
            });

            function isValidName(name) {
                return name.trim().length >= 2;
            }
            function validateEmail($email) {
                const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                return emailReg.test( $email );
            }


            enableFastFeedback($("#person"));

            function enableFastFeedback(formElement) {
                const firstName = formElement.find("#firstName");
                const lastName = formElement.find("#lastName");
                const email = formElement.find("#email");


                firstName.blur(function () {
                    const name = $(this).val();
                    highlight($(this), isValidName(name));
                    if (!isValidName(name)) {
                        $("#firstName-feedback").text("Please enter at least two characters");
                    } else {
                        $("#firstName-feedback").text("");
                    }
                });

                lastName.blur(function () {
                    const name = $(this).val();
                    highlight($(this), isValidName(name));
                    if (!isValidName(name)) {
                        $("#lastName-feedback").text("Please enter at least two characters");
                    } else {
                        $("#lastName-feedback").text("");
                    }
                });
                email.blur(function () {
                    const email = $(this).val();
                    highlight($(this), validateEmail(email));
                    if (!validateEmail(email)) {
                        $("#email-feedback").text("Please enter a valid email");
                    } else {
                        $("#email-feedback").text("");
                    }
                })

            }

            function highlight(element, isValid) {
                let color = "#811";  // red
                if (isValid) {
                    color = "#181";  // green
                }

                element.css("box-shadow", "0 0 4px " + color);
            }


        });
    </script>
</head>
<body>
<h1>
    Add a Person
</h1>


<c:url var="addAction" value="/person/add"></c:url>

<form:form action="${addAction}" commandName="person">
    <table>
        <c:if test="${!empty person.id}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="firstName">
                    <spring:message text="First Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="firstName"/>
            </td>
            <td><span id="firstName-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <form:label path="lastName">
                    <spring:message text="Last Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="lastName"/>
            </td>
            <td><span id="lastName-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <form:label path="age">
                    <spring:message text="Age"/>
                </form:label>
            </td>
            <td>
                <form:input path="age"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="email">
                    <spring:message text="E-mail"/>
                </form:label>
            </td>
            <td>
                <form:input path="email"/>
            </td>
            <td><span id="email-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <form:label path="country">
                    <spring:message text="Country"/>
                </form:label>
            </td>
            <td>
                <form:input path="country"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <c:if test="${!empty person.id}">
                    <input type="submit"
                           value="<spring:message text="Edit Person"/>"/>
                </c:if>
                <c:if test="${empty person.id}">
                    <input type="submit"
                           value="<spring:message text="Add Person"/>"/>
                </c:if>
            </td>
        </tr>
    </table>
</form:form>
<br>
<h3>Persons List</h3>
<c:if test="${!empty persons}">
    <table class="tg">
        <tr>
            <th width="80">ID</th>
            <th width="120">First Name</th>
            <th width="120">Last Name</th>
            <th width="80">Age</th>
            <th width="120">E-mail</th>
            <th width="120">Country</th>
            <th width="60">Edit</th>
            <th width="60">Delete</th>
        </tr>
        <c:forEach items="${persons}" var="person">
            <tr>
                <td>${person.id}</td>
                <td>${person.firstName}</td>
                <td>${person.lastName}</td>
                <td>${person.age}</td>
                <td>${person.email}</td>
                <td>${person.country}</td>
                <td><a href="<c:url value='/edit/${person.id}' />">Edit</a></td>
                <td><a href="<c:url value='/remove/${person.id}' />">Delete</a></td>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>