package com.giga.model;


import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PersonDTO {

    private Long id;

    private String firstName;
    private String lastName;
    private int age;
    private String email;
    private String country;

}
