package com.giga.mapper;

import com.giga.entity.Person;
import com.giga.model.PersonDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonMapper {

    @Mapping(target = "firstName", source = "name")
    @Mapping(target = "lastName", source = "surname")
    PersonDTO map(Person consumer);

    @InheritInverseConfiguration
    Person map(PersonDTO consumer);


    List<PersonDTO> map(List<Person> employees);


}
