package com.giga.service;

import com.giga.model.PersonDTO;

import java.util.List;

public interface PersonService {
    void save(PersonDTO entity);
    void updatePerson(PersonDTO person);
    void delete(Long person);
    List<PersonDTO> findAll();
    PersonDTO findById(Long id);


}
