package com.giga.service;

import com.giga.dao.PersonDAOImpl;
import com.giga.entity.Person;
import com.giga.mapper.PersonMapper;
import com.giga.model.PersonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class  PersonServiceImpl implements PersonService{

    private final PersonDAOImpl personDAOimpl;
    private final PersonMapper personMapper;

    public PersonServiceImpl(PersonDAOImpl personDAOimpl, PersonMapper personMapper) {
        this.personDAOimpl = personDAOimpl;
        this.personMapper = personMapper;
    }

    @Override
    @Transactional
    public void save(PersonDTO entity) {
        personDAOimpl.save(personMapper.map(entity));
    }

    @Override
    @Transactional
    public List<PersonDTO> findAll() {
        List<Person> persons = personDAOimpl.findAll();
        return personMapper.map(persons);
    }

    @Override
    @Transactional
    public PersonDTO findById(Long id) {

        return personMapper.map(personDAOimpl.findById(id));
    }
    @Override
    @Transactional
    public void updatePerson(PersonDTO p) {
        this.personDAOimpl.updatePerson(personMapper.map(p));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        this.personDAOimpl.delete(id);
    }
}
