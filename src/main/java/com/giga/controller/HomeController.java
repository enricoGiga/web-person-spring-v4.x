package com.giga.controller;

import com.giga.model.PersonDTO;
import com.giga.service.PersonService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@Controller
public class HomeController {
    private final PersonService personService;

    public HomeController(PersonService personService) {
        this.personService = personService;
    }


    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public String home(Model model) {
        List<PersonDTO> persons = this.personService.findAll();
        model.addAttribute("person", new PersonDTO());
        model.addAttribute("persons", persons);

        return "persons";
    }

    @RequestMapping(value = "/person/add", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("personDTO") PersonDTO personDTO) {

        if (personDTO.getId() == null) {
            //new person, add it
            this.personService.save(personDTO);
        } else {
            //existing person, call update
            this.personService.updatePerson(personDTO);
        }

        return "redirect:/persons";

    }

    @RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") Long id) {

        this.personService.delete(id);
        return "redirect:/persons";
    }

    @RequestMapping("/edit/{id}")
    public String editPerson(@PathVariable("id") Long id, Model model) {
        model.addAttribute("person", this.personService.findById(id));
        model.addAttribute("listPersons", this.personService.findAll());
        return "persons";
    }
}
