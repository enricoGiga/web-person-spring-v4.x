package com.giga.dao;

import com.giga.entity.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonDAOImpl implements PersonDAO<Person, Long> {
    private static final Logger logger = LoggerFactory.getLogger(PersonDAOImpl.class);
    private final SessionFactory sessionFactory;


    public PersonDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    @Override
    public void updatePerson(Person p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
        logger.info("Person updated successfully, Person Details=" + p);
    }

    @Override
    public void save(Person entity) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(entity);
    }


    public List<Person> findAll() {

        Session session = this.sessionFactory.getCurrentSession();
        return (List<Person>) session.createQuery("from Person").list();
    }

    @Override
    public Person findById(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        return (Person) session.load(Person.class, id);
    }

    @Override
    public void delete(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Person ent = (Person) session.load(Person.class, id);
        session.delete(ent);
    }
}
