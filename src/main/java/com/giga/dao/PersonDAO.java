package com.giga.dao;

import java.io.Serializable;
import java.util.List;
 
public interface PersonDAO<T, Id extends Serializable> {
	 void updatePerson(T p);
	 void save(T entity);
	 void delete(Id p);
	 List<T> findAll();
	 T findById(Id id);
}
