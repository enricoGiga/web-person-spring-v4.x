CREATE TABLE Person
(
    id      serial      NOT NULL,
    name    varchar(20) NOT NULL,
    surname varchar(50) NOT NULL,
    age     integer     not null,
    email   varchar(60) not null,
    country varchar(20) not null,
    PRIMARY KEY (id)
);